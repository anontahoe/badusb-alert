BadUSB-Alert
============

Gives a warning when BadUSB-infected devices try to emulate mouse and keyboard to take over your computer.  
There is also a mode that prevents those devices from doing anything until you react to the warnings.  
The problem is that there is no way to recognize which device is malicious and which is a legit keyboard so you need to whitelist every input device you want to use. Just plug it in and follow the instructions.  

#### Dependencies
*	screen (unless you set blockNewDevices=1)
*	flock (a tool to use files for mutex locking)

#### Installation
*	Move the udev rule in etc/udev/rules.d/ to /etc/udev/rules.d/
*	Move the binaries to /usr/bin/ and make them executable
*	Restart/refresh udev
*	Unplug and plug in your keyboard, whitelist it and do the same with other input devices
*	Optionally and currently not recommended: edit badusb-starter.sh and set blockNewDevices to '1'

#### Notes
*	BadUSB-Alert doesn't work on Xubuntu or any other buntu and neither on most other linux distros because their 'w' command does not return the values i need. Generally i believe login managers are the problem.
*	The whitelist is stored in /var/lib/badusb-alert/ and logs/debug output are stored in /tmp/badusb-alert/
*	If you have any questions, email me at anontahoe(at)anonmail.pwnz.org

#### TODO
*	fix bugs in blockNewDevices=1 mode
*	create a version that can run on headless machines
*	make compatible with login managers as used by most users, right now it's a collection of pathetic hacks
