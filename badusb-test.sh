#!/bin/bash

#   Copyright (C) 2015  anontahoe
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


#ID_PATH_TAG="pathtag" ID_MODEL_ID="idmodelid" ID_SERIAL="idserial" ID_VENDOR="idvendor" DEVLINKS="devlinks1 devlinks2" badusb-starter.sh


DEVLINKS="/dev/serial/by-id/usb-067b_2303-if00-port0 /dev/serial/by-path/pci-0000:00:1d.0-usb-0:1.4:1.0-port0" \
DEVNAME="/dev/ttyUSB0" \
DEVPATH="/devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.4/2-1.4:1.0/ttyUSB0/tty/ttyUSB0" \
ID_BUS="usb" \
ID_MM_CANDIDATE="1" \
ID_MODEL="2303" \
ID_MODEL_ENC="2303" \
ID_MODEL_FROM_DATABASE="PL2303 Serial Port" \
ID_MODEL_ID="2303" \
ID_PATH="pci-0000:00:1d.0-usb-0:1.4:1.0" \
ID_PATH_TAG="pci-0000_00_1d_0-usb-0_1_4_1_0" \
ID_REVISION="0202" \
ID_SERIAL="067b_2303" \
ID_TYPE="hid" \
ID_USB_DRIVER="pl2303" \
ID_USB_INTERFACES=":ff0000:" \
ID_USB_INTERFACE_NUM="00" \
ID_VENDOR="067b" \
ID_VENDOR_ENC="067b" \
ID_VENDOR_FROM_DATABASE="Prolific Technology, Inc." \
ID_VENDOR_ID="067b" \
MAJOR="188" \
MINOR="0" \
SUBSYSTEM="tty" \
UDEV_LOG="3" \
USEC_INITIALIZED="10665628223" \
badusb-starter.sh














