#!/bin/bash

#   Copyright (C) 2015  anontahoe
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


# this file runs as root


# set blockNewDevices to '1' to block devices from performing any actions until they are whitelisted or ignored
# this means your keyboard won't work so whitelist that before you change this value
# requires screen to work
blockNewDevices=0

function log
{
	(
		flock -x $FDlog
		echo "$1" >> "$logfile"
	){FDlog}>"$logfile.lock"
}

function setupfiles
{
	tmpdir="/tmp/badusb-alert"
	#rm -r "$tmpdir"
	mkdir -p "$tmpdir"

	logfile="$tmpdir/alert"
	touch "$logfile"
	touch "$logfile.lock"

	devqueue="$tmpdir/devqueue"
	touch "$devqueue"
	touch "$devqueue.count"
	touch "$devqueue.lock"

	currentDevice="$tmpdir/currentdevice"
	touch "$currentDevice"
	touch "$currentDevice.lock"

	daemonLock="$tmpdir/daemon.lock"
	touch "$daemonLock"

	chmod ugo+r -R "$tmpdir"
	chmod ugo+x "$tmpdir"
	chmod o-w -R "$tmpdir"

	whitelistdir="/var/lib/badusb-alert"
	whitelist="$whitelistdir/whitelist"
	#rm -r "$whitelistdir"
	mkdir -p "$whitelistdir"
	touch "$whitelist"
	chmod ugo+r -R "$whitelistdir"
	chmod ugo+x "$whitelistdir"
	chmod o-w -R "$whitelistdir"

	fifo="$tmpdir/fifo"
	mkfifo "$fifo"
	chmod o-r "$fifo"
	chmod o+w "$fifo"
}


setupfiles

(
	flock -x $FD

	read deviceCount < "$devqueue.count"
	((deviceCount++))
	echo "$deviceCount" > "$devqueue.count"
	echo "$deviceCount" >> "$devqueue"
	if [[ "$DEVLINKS" != "" ]]; then
		echo "$DEVLINKS" >> "$devqueue"
	else
		echo "empty empty" >> "$devqueue"
	fi
	echo "$DEVNAME" >> "$devqueue"
	echo "$DEVPATH" >> "$devqueue"
	echo "$ID_BUS" >> "$devqueue"
	echo "$ID_MM_CANDIDATE" >> "$devqueue"
	echo "$ID_MODEL" >> "$devqueue"
	echo "$ID_MODEL_ENC" >> "$devqueue"
	echo "$ID_MODEL_FROM_DATABASE" >> "$devqueue"
	echo "$ID_MODEL_ID" >> "$devqueue"
	echo "$ID_PATH" >> "$devqueue"
	echo "$ID_PATH_TAG" >> "$devqueue"
	echo "$ID_REVISION" >> "$devqueue"
	echo "$ID_SERIAL" >> "$devqueue"
	echo "$ID_TYPE" >> "$devqueue"
	echo "$ID_USB_DRIVER" >> "$devqueue"
	echo "$ID_USB_INTERFACES" >> "$devqueue"
	echo "$ID_USB_INTERFACE_NUM" >> "$devqueue"
	echo "$ID_VENDOR" >> "$devqueue"
	echo "$ID_VENDOR_ENC" >> "$devqueue"
	echo "$ID_VENDOR_FROM_DATABASE" >> "$devqueue"
	echo "$ID_VENDOR_ID" >> "$devqueue"
	echo "$MAJOR" >> "$devqueue"
	echo "$MINOR" >> "$devqueue"
	echo "$SUBSYSTEM" >> "$devqueue"
	echo "$UDEV_LOG" >> "$devqueue"
	echo "$USEC_INITIALIZED" >> "$devqueue"

	if [[ blockNewDevices -eq 0 ]]; then
		log "badusb-starter launching non-blocking badusb-daemon..."
		XAUTHORITY="$XAUTHORITY" DISPLAY="$DISPLAY" screen -d -m -S "badusb-daemon" "/usr/bin/badusb-daemon.sh"
	fi
){FD}<"$devqueue.lock"

if [[ blockNewDevices -eq 1 ]]; then
	log "badusb-starter launching blocking badusb-daemon..."
	XAUTHORITY="$XAUTHORITY" DISPLAY="$DISPLAY" "/usr/bin/badusb-daemon.sh"
fi

exit
