#!/bin/bash

#   Copyright (C) 2015  anontahoe
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


# this file runs with root privileges

function log {
	(
		flock -x "$FDlog"
		echo "$$ $1" >> "$tmpfile"
){FDlog}>"$tmpfile.lock"
}

function Initialize {
	tmpdir="/tmp/badusb-alert"
	tmpfile="$tmpdir/alert"
	devqueue="$tmpdir/devqueue"
	currentDevice="$tmpdir/currentdevice"
	daemonLock="$tmpdir/daemon.lock"
	fifo="$tmpdir/fifo"

	whitelistdir="/var/lib/badusb-alert"
	whitelist="$whitelistdir/whitelist"

	clientCount=0
	terminal="lxterminal"

	log "daemon started"

	i=1
	deviceId=$((i++))
	DEVLINKS=$((i++))
	DEVNAME=$((i++))
	DEVPATH=$((i++))
	ID_BUS=$((i++))
	ID_MM_CANDIDATE=$((i++))
	ID_MODEL=$((i++))
	ID_MODEL_ENC=$((i++))
	ID_MODEL_FROM_DATABASE=$((i++))
	ID_MODEL_ID=$((i++))
	ID_PATH=$((i++))
	ID_PATH_TAG=$((i++))
	ID_REVISION=$((i++))
	ID_SERIAL=$((i++))
	ID_TYPE=$((i++))
	ID_USB_DRIVER=$((i++))
	ID_USB_INTERFACES=$((i++))
	ID_USB_INTERFACE_NUM=$((i++))
	ID_VENDOR=$((i++))
	ID_VENDOR_ENC=$((i++))
	ID_VENDOR_FROM_DATABASE=$((i++))
	ID_VENDOR_ID=$((i++))
	MAJOR=$((i++))
	MINOR=$((i++))
	SUBSYSTEM=$((i++))
	UDEV_LOG=$((i++))
	USEC_INITIALIZED=$((i++))
	i=1
}

function TryGetLock {
	(
		flock -x "$FD"

		log "	locking $daemonLock..."
		flock -x -n "$FDdaemon"
		locked="$?"
		if [[ locked -ne 0 ]]; then
			log "		couldn't get lock, daemon already running. exiting this instance."
			echo 1
			exit
			return
		fi
		echo 0
	){FD}<"$devqueue.lock"
}

function GetFileLineCount {
	len=$(wc -l "$1" | awk '{ print $1; }')
	echo "$len"
}

function ReadCurrentDeviceInfo {
	for i in `seq 1 27`; do
		line="$(sed "$i""q;d" < "$currentDevice")"
		deviceInfoMap[i]="$line"
	done
}

function SanitizeName {
  echo "$(sed -e "s/[^A-Za-z0-9.\/-]/_/g" <<< "$1")"
}

function CreateWhitelistName {
	vendor="$(SanitizeName "${deviceInfoMap[ID_VENDOR]}")"
	model="$(SanitizeName "${deviceInfoMap[ID_MODEL]}")"
	devpath="$(SanitizeName "${deviceInfoMap[DEVPATH]}")"
	idpathtag="$(SanitizeName "${deviceInfoMap[ID_PATH_TAG]}")"
	read devlinks1 devlinks2 <<< "${deviceInfoMap[DEVLINKS]}"
	devlinks1="$(SanitizeName "$devlinks1")"
	devlinks2="$(SanitizeName "$devlinks2")"
	whiteListString="$vendor:$model:$idpathtag:$devlinks1"
	echo "$whiteListString"
}

function IsDeviceWhitelisted {
	log "	checking if device #${deviceInfoMap[ID_TYPE]} is whitelisted"
	whiteListString="$(CreateWhitelistName)"
	log "		whiteListString: $whiteListString"

	while read whitelistedDeviceString; do
		if [[ "$whiteListString" == "$whitelistedDeviceString" ]]; then
			echo 0
			return
		fi
	done < "$whitelist"

	echo 1
}

function UpdateCurrentDevice {
	(
		flock -x "$FD"

		notYetWhitelisted=0
		while [[ notYetWhitelisted -eq 0 ]]; do
			log "updating current device..."
			devqueueLength=$(GetFileLineCount "$devqueue")
			if [[ devqueueLength -eq 0 ]]; then
				log "	no more devices to process"
				echo 1
				return
			fi

			log "	moving device info from \$devqueue to \$currentDevice"
			currentDeviceInfo="$(head -n 27 "$devqueue")"
			remainingDevices="$(tail -n $((devqueueLength - 27)) "$devqueue")"
			echo "$currentDeviceInfo" > "$currentDevice"
			if [[ "$remainingDevices" != "" ]]; then
				echo "$remainingDevices" > "$devqueue"
			else
				echo -n "" > "$devqueue"
			fi

			# read current device info into array
			ReadCurrentDeviceInfo

			if [[ "${deviceInfoMap[ID_TYPE]}" != "hid" ]]; then
				log "	not a human input device, ID_TYPE should be 'hid'. skipping."
				continue
			fi

			notYetWhitelisted="$(IsDeviceWhitelisted "$currentDeviceInfo")"
			if [[ notYetWhitelisted -eq 0 ]]; then
				log "		already whitelisted"
			else
				log "		not yet whitelisted"
			fi
		done

		echo 0
	){FD}<"$devqueue.lock"
}

function GetUserName {
	# get username from a path like /home/<user>/whatever
    IFS=:'/' read -a pathFields <<< "$1"
    echo "${pathFields[2]}"
}

function StartClient {
	user="$1"
	dispNum="$2"
	#xauthority="$3"
	xauthority="/home/$user/.Xauthority"

	pidFifo="$tmpdir/pidfile-display-$dispNum"
	mkfifo "$pidFifo"
	chown "$user" "$pidFifo"
	chmod go-r "$pidFifo"
	chmod go-w "$pidFifo"

	if [[ "$user" != "genad" ]]; then
		return
	fi

	su "$user" -c "XAUTHORITY=$xauthority \
					DISPLAY=:$dispNum \
					$terminal -e /usr/bin/badusb-useralert.sh $user $dispNum"

	log "client for $user on display $display started, waiting for PID"
	displayPidMap[dispNum]=0
	displayPidMap[dispNum]="$(timeout 1 cat $pidFifo)"
	log "	pid is ${displayPidMap[dispNum]}"
}

function CheckClientsRunning {
	# run the 'w' command to get list of X sessions
	# the output of the relevant lines should look like
	#	USER     TTY        LOGIN@   IDLE   JCPU   PCPU WHAT
	#	anon    tty1      14May15 10days 14:03   0.00s xinit /home/anon/.xinitrc -- /etc/X11/xinit/xserverrc :0 -auth /home/anon/.serverauth.2907
	while read user tty login idle jcpu pcpu what xinitrc minusminus xserverrc display auth serverauth; do
		if [[ "$what" != "xinit" ]]; then
			continue
		fi

		# remove the ':' in ':0'
		dispNumber=$(cut -c2- <<< "$display")

		# launch client on this display if we haven't launched one before
		if [[ -z displayPidMap["$dispNumber"] ]]; then
			log "launching client on display $display for the first time"
			StartClient "$user" "$dispNumber" "$serverauth"
			continue
		fi

		# log "checking if client pid ${displayPidMap[$dispNumber]} is still running"
		kill -0 ${displayPidMap["$dispNumber"]}
		stillRunning=$?
		if [[ stillRunning -ne 0 ]]; then
			userName=$(GetUserName "$xinitrc")
			StartClient "$userName" "$dispNumber" "$serverauth"
		fi
	done <<< "$(w)"
}

function WhiteListDevice() {
	(
		flock -x "$FD"

		oldDeviceId="$1"
		ReadCurrentDeviceInfo
		if [[ deviceInfoMap[deviceId] -ne oldDeviceId ]]; then
			log "	already taken care of, current device id: ${deviceInfoMap[deviceId]}"
			echo 1
			return
		fi

		whiteListString="$(CreateWhitelistName)"
		log "	whiteListString: $whiteListString"
		echo "$whiteListString" >> "$whitelist"
		echo 0

	){FD}<"$devqueue.lock"
}

function IgnoreDevice() {
	(
		flock -x "$FD"

		deviceId="$1"
		read currentDeviceId < "$currentDevice"
		if [[ currentDeviceId -ne deviceId ]]; then
			log "	already taken care of"
			echo 1
			return
		fi

		echo 0
	){FD}<"$devqueue.lock"
}

function CheckClientCommands {
	while read clientId command args; do
		if [[ "$command" == "" ]]; then
			continue
		fi
		case "$command" in
		"log")
			log "$args"
		;;
		"whitelist")
			log "$clientId whitelisting device #$args"
			whitelistFailed="$(WhiteListDevice "$args")"
			if [[ "$whitelistFailed" -eq 0 ]]; then
				echo "$(UpdateCurrentDevice)"
			fi
		;;
		"ignore")
			log "$clientId ignoring device #$args"
			ignoreFailed="$(IgnoreDevice "$args")"
			if [[ "$ignoreFailed" -eq 0 ]]; then
				echo "$(UpdateCurrentDevice)"
			fi
		;;
		*)
			log "unknown command $clientId $command $args"
		;;
		esac
	done <<< "$(timeout 3 cat $fifo)"
}

function KillRemainingClients {
	log "killing remaining clients..."
	for i in "${!displayPidMap[@]}"; do
		log "	killing pid ${displayPidMap[$i]}"
		kill "${displayPidMap[$i]}"
	done
}

function ShutDown {
	KillRemainingClients
	(
		# lock the device queue before unlocking daemon lock to prevent race conditions
		flock -x "$FD"
		log "daemon shutting down..."
		log ""; log ""; log ""
		flock -u "$FDdaemon"
	){FD}<"$devqueue.lock"
	exec {FDdaemon}>&-
	exit
}


Initialize
# get file descriptor to daemon lock file
exec {FDdaemon}>"$daemonLock"
lockFailed="$(TryGetLock)"
if [[ lockFailed -ne 0 ]]; then
	exit
fi
#ShutDown
devqueueEmpty="$(UpdateCurrentDevice)"
while [[ devqueueEmpty -eq 0 ]]; do
	CheckClientsRunning
	devqueueEmpty="$(CheckClientCommands)"
	sleep 1
done

ShutDown













