#!/bin/bash

#   Copyright (C) 2015  anontahoe
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


# this file is executed with user privileges

user="$1"
dispNum="$2"
pid="$$"

function log {
	#echo "$1"
	echo "$dispNum log $1" >> "$fifo"
}

function SendCommand {
	command="$1"
	if [[ "$2" != "" ]]; then
		args="$2"
	else
		args="${deviceInfoMap[deviceId]}"
	fi
	echo "$dispNum $command $args" >> "$fifo"
}

function Initialize {
	tmpdir="/tmp/badusb-alert"
	tmpfile="$tmpdir/alert"
	devqueue="$tmpdir/devqueue"
	currentDevice="$tmpdir/currentdevice"
	fifo="$tmpdir/fifo"

	# pass pid to the daemon process
	pidFifo="$tmpdir/pidfile-display-$dispNum"
	echo "$pid" > "$pidFifo"
	echo "pid: $pid"

	i=1
	deviceId=$((i++))
	DEVLINKS=$((i++))
	DEVNAME=$((i++))
	DEVPATH=$((i++))
	ID_BUS=$((i++))
	ID_MM_CANDIDATE=$((i++))
	ID_MODEL=$((i++))
	ID_MODEL_ENC=$((i++))
	ID_MODEL_FROM_DATABASE=$((i++))
	ID_MODEL_ID=$((i++))
	ID_PATH=$((i++))
	ID_PATH_TAG=$((i++))
	ID_REVISION=$((i++))
	ID_SERIAL=$((i++))
	ID_TYPE=$((i++))
	ID_USB_DRIVER=$((i++))
	ID_USB_INTERFACES=$((i++))
	ID_USB_INTERFACE_NUM=$((i++))
	ID_VENDOR=$((i++))
	ID_VENDOR_ENC=$((i++))
	ID_VENDOR_FROM_DATABASE=$((i++))
	ID_VENDOR_ID=$((i++))
	MAJOR=$((i++))
	MINOR=$((i++))
	SUBSYSTEM=$((i++))
	UDEV_LOG=$((i++))
	USEC_INITIALIZED=$((i++))
	i=1

	for i in `seq 1 27`; do
		deviceInfoMap[i]=""
	done
}

function ReadCurrentDeviceInfo {
	exec {FDdevqueue}<"$devqueue.lock"
	flock -x "$FDdevqueue"

	for i in `seq 1 27`; do
		line="$(sed "$i""q;d" < "$currentDevice")"
		deviceInfoMap[i]="$line"
	done

	flock -o -u "$FDdevqueue"
	exec {FDdevqueue}<&-

	i=1
	echo "deviceId:
	${deviceInfoMap[$((i++))]}"
	echo "DEVLINKS:
	${deviceInfoMap[$((i++))]}"
	echo "DEVNAME:
	${deviceInfoMap[$((i++))]}"
	echo "DEVPATH:
	${deviceInfoMap[$((i++))]}"
	echo "ID_BUS:
	${deviceInfoMap[$((i++))]}"
	echo "ID_MM_CANDIDATE:
	${deviceInfoMap[$((i++))]}"
	echo "ID_MODEL:
	${deviceInfoMap[$((i++))]}"
	echo "ID_MODEL_ENC:
	${deviceInfoMap[$((i++))]}"
	echo "ID_MODEL_FROM_DATABASE:
	${deviceInfoMap[$((i++))]}"
	echo "ID_MODEL_ID:
	${deviceInfoMap[$((i++))]}"
	echo "ID_PATH:
	${deviceInfoMap[$((i++))]}"
	echo "ID_PATH_TAG:
	${deviceInfoMap[$((i++))]}"
	echo "ID_REVISION:
	${deviceInfoMap[$((i++))]}"
	echo "ID_SERIAL:
	${deviceInfoMap[$((i++))]}"
	echo "ID_TYPE:
	${deviceInfoMap[$((i++))]}"
	echo "ID_USB_DRIVER:
	${deviceInfoMap[$((i++))]}"
	echo "ID_USB_INTERFACES:
	${deviceInfoMap[$((i++))]}"
	echo "ID_USB_INTERFACE_NUM:
	${deviceInfoMap[$((i++))]}"
	echo "ID_VENDOR:
	${deviceInfoMap[$((i++))]}"
	echo "ID_VENDOR_ENC:
	${deviceInfoMap[$((i++))]}"
	echo "ID_VENDOR_FROM_DATABASE:
	${deviceInfoMap[$((i++))]}"
	echo "ID_VENDOR_ID:
	${deviceInfoMap[$((i++))]}"
	echo "MAJOR:
	${deviceInfoMap[$((i++))]}"
	echo "MINOR:
	${deviceInfoMap[$((i++))]}"
	echo "SUBSYSTEM:
	${deviceInfoMap[$((i++))]}"
	echo "UDEV_LOG:
	${deviceInfoMap[$((i++))]}"
	echo "USEC_INITIALIZED:
	${deviceInfoMap[$((i++))]}"

}

function GetFileLineCount {
	len=$(wc -l "$1" | awk '{ print $1; }')
	echo "$len"
}

function GetCurrentDeviceId() {
	read currentDeviceId < "$currentDevice"
	echo "$currentDeviceId"
}

function WaitDataAvailable {
	(
		flock -x "$FDdevqueue"

		# check if there is a device to process right now
		newDeviceId="$(GetCurrentDeviceId)"
		if [[ newDeviceId -ne deviceInfoMap[deviceId] ]]; then
			echo 0
			return
		fi

		# check if there's gonna be a device soon
		devqueueLength="$(GetFileLineCount "$devqueue")"
		if [[ devqueueLength -ne 0 ]]; then
			sleep 0.1
			echo 1
			return
		fi

		echo 2

	){FDdevqueue}<"$devqueue.lock"
}

function AskUser {
	answered=0
	while [[ answered -eq 0 ]]; do
		echo "Enter 'yes' to whitelist the device"
		echo "Enter 'no' to show this warning when the device is plugged in again"
		echo -n "Whitelist the device? "
		read answer

		if [[ "$answer" == "yes" ]]; then
			SendCommand "whitelist"
			answered=1
		elif [[ "$answer" == "no" ]]; then
			SendCommand "ignore"
			answered=1
		fi
	done
}



Initialize

noMoreData="$(WaitDataAvailable)"
while [[ noMoreData -lt 2 ]]; do
	echo "noMoreData: $noMoreData"
	if [[ noMoreData -eq 1 ]]; then
		echo "waiting for more data..."
		noMoreData="$(WaitDataAvailable)"
		continue
	fi

	echo ""
	echo "A human input device was connected over USB."
	echo "Press enter to see information"
	read key
	ReadCurrentDeviceInfo
	echo ""
	echo "If you think the device is infected you MUST unplug it before continuing."
	AskUser

	noMoreData="$(WaitDataAvailable)"
done

echo "exiting..."
sleep 1
exit 0










